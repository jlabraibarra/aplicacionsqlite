<meta charset="ISO-8859-1">
<div class="container" style="max-width: 90%; padding:0 5% 0 5%;">
<p align="center"><img src="https://androiddeveloperbrasil.files.wordpress.com/2015/07/sqlitecapa.png" width="80%"></p>
<hr>
<h2>Ídice.</h2>
<ol>
    <a href="#one">
        <li>Tecnologías utilizadas en el sistema.</li>
    </a>
    <a href="#two">
        <li>Sobre la aplicación.</li>
    </a>
    <a href="#three">
        <li>Sobre modulos y submodulos.</li>
    </a>
</ol>
<hr id="one">
<h1>Tecnologías utilizadas para el desarrollo.</h1>
<center>
<img src="http://www.marketing-professionnel.fr/wp-content/uploads/2012/01/android-google.png" width="200px"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/SQLite370.svg/1024px-SQLite370.svg.png" width="200px">
</center>
<hr id="two">
<h1>Sobre la aplicación</h1>
<p style="text-align: justify;">Esta aplicación esta desarrollada en adroid estudio, para ver el funcionamiento de una base de datos interna, utilizando el motor de BD SQLite, a continuacion se muestran sus principales funciones:</p>

<h4>Modulo Insertar.</h4>
<ul>
    <li>Crear un nuevo usuario.</li>
</ul>
<h4>Modulo de consultar</h4>
<ul>
    <li>Consultar información de usuario.</li>
    <li>Modificar informacion de usuario.</li>
    <li>Eliminar usuario.</li>
</ul>
<h4>Modulo lista.</h4>
<ul>
    <li>Lista de informacion de usuarios.</li>
</ul>
<h4>Modulo de Ventas</h4>
<ul>
    <li>Submodulo Venta.</li>
</ul>

<hr id="three">
<h1>Sobre los modulos y submodulos</h1>
<h3>Bienvenida.</h3>
<center>
    <p>Pantalla de bienvenida.</p>
    <img src="#" width="90%">
    <p>Pantalla de menu.</p>
    <img src="#" width="90%">
</center>
<h3>Modulo Insertar.</h3>
<center>
    <p>Pantalla de insertar.</p>
    <img src="#" width="90%">
</center>
<h3>Modulo busacar.</h3>
<center>
    <p>Pantalla de insertar.</p>
    <img src="#" width="90%">
    <p>Pantalla de eliminar.</p>
    <img src="#" width="90%">
    <p>Pantalla de modificar.</p>
    <img src="#" width="90%">
</center>
<h3>Modulo Insertar.</h3>
<center>
    <p>Pantalla de lista.</p>
    <img src="#" width="90%">
</center>
<hr>
    <h1>Redes.</h1>
    <center>
    <div id="share">
        <a rel="nofollow" title="Forever programación, página del sitio en Facebook sobre informática e internet" href="https://www.facebook.com/foreverprogramacion" target="_blank">
        <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 32 32"><path fill="#3B5998" d="M0 0h32v32H0z"></path><path fill="#FFF" d="M22.08 32V19.607h4.16l.62-4.83h-4.78v-3.083c0-1.398.388-2.352 2.393-2.352h2.56V5.02c-.443-.058-1.962-.19-3.728-.19-3.688 0-6.213 2.25-6.213 6.385v3.562h-4.17v4.83h4.17V32h4.987z"></path></svg></a>
        <a rel="nofollow" href="#" title="@jlabra, cuenta del sitio en Twitter" target="_blank">
        <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 32 32"><path fill="#55ACEE" d="M0 0h32v32H0z"></path><path fill="#FFF" d="M28 8.557c-.884.39-1.833.656-2.828.775 1.017-.608 1.798-1.573 2.166-2.725-.953.567-2.006.976-3.13 1.194-.896-.956-2.176-1.554-3.593-1.554-2.72 0-4.924 2.206-4.924 4.925 0 .387.045.763.13 1.125-4.094-.208-7.724-2.168-10.15-5.147-.423.726-.667 1.573-.667 2.476 0 1.71.87 3.214 2.19 4.1-.806-.027-1.564-.25-2.23-.616v.06c0 2.39 1.7 4.378 3.952 4.83-.414.113-.85.172-1.297.172-.317 0-.625-.03-.927-.086.63 1.956 2.447 3.38 4.6 3.42-1.685 1.318-3.808 2.107-6.114 2.107-.398 0-.79-.023-1.175-.068 2.18 1.396 4.768 2.213 7.55 2.213 9.056 0 14.01-7.506 14.01-14.012 0-.213-.005-.426-.015-.637.96-.694 1.795-1.56 2.455-2.55z"></path></svg></a>
        <a rel="nofollow" href="#" title="#, página del sitio en Google+" target="_blank">
        <svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 32 32"><path fill="#DD4B39" d="M0 0h32v32H0z"></path><g fill="#FFF"><path d="M27 15h-2v-2h-2v2h-2v2h2v2h2v-2h2M12 15v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83C15.47 9.69 13.89 9 12 9c-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16H12z"></path></g></svg></a>
    </div><br>
    <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fforeverprogramacion&width=450&layout=standard&action=like&size=large&show_faces=true&share=true&height=35&appId=511587038884537" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
    <br>
    </center>
<hr>
</div>  
<footer>
    <center>
        <p>Desing by <a href="http://proforever.webcindario.com" target="_blank">Jesús Labra</a> &copy; 2019</p>
    </center>
</footer>
