package foreverprogramacion.aplicacionsqllite.Utilidades;

public class Utilidades {

    public static String TABLA_PERONAS = "personas";
    public static String CAMPO_ID = "id";
    public static String CAMPO_NOMBRE = "nombre";
    public static String CAMPO_APELLIDO = "apellido";
    public static String CAMPO_TELEFONO = "telefono";

    //Scrip para crear la tabla personas
    public static final String CREAR_TABLA_PERSONAS = "CREATE TABLE "+TABLA_PERONAS+" ("+CAMPO_ID+" INTEGER, "+CAMPO_NOMBRE+" TEXT, "+CAMPO_APELLIDO+" TEXT, "+CAMPO_TELEFONO+" TEXT);";

}
