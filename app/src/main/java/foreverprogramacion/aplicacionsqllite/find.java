package foreverprogramacion.aplicacionsqllite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import foreverprogramacion.aplicacionsqllite.Utilidades.Utilidades;

public class find extends AppCompatActivity {

    EditText campoId, nombre, apellido, telefono;
    conexionHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find);

        conn = new conexionHelper(this,"bd_personas",null,1);

        campoId = (EditText) findViewById(R.id.idFind);
        nombre = (EditText) findViewById(R.id.nombreFind);
        apellido = (EditText) findViewById(R.id.apellidoFind);
        telefono = (EditText) findViewById(R.id.telefonoFind);
    }

    public void cosultar(View vw){
        consultar();
    }

    public void eliminar(View vew){
        if (campoId.equals("")){
            Toast.makeText(this,"El campo ID no debe estar vacio",Toast.LENGTH_SHORT).show();
        }else{eliminarUsuario();}
    }

    public void modificar(View view){
        if (campoId.equals("") && nombre.equals("") && apellido.equals("") && telefono.equals("")){
            Toast.makeText(this,"Los campos no deben estar vacios",Toast.LENGTH_SHORT).show();
        }else{modificarUsuario();}
    }

    private void eliminarUsuario() {
        //Usar la base de datos
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {campoId.getText().toString()};

        db.delete(Utilidades.TABLA_PERONAS,Utilidades.CAMPO_ID+"=?",parametros);

        Toast.makeText(this,"Los datos fueron eliminados!",Toast.LENGTH_SHORT).show();
        campoId.setText("");
        apellido.setText("");
        telefono.setText("");
        nombre.setText("");
        db.close();
    }

    private void modificarUsuario(){
        //Usar la base de datos
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {campoId.getText().toString()};
        //Datos a actualizar
        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_NOMBRE,nombre.getText().toString());
        values.put(Utilidades.CAMPO_APELLIDO,apellido.getText().toString());
        values.put(Utilidades.CAMPO_TELEFONO,telefono.getText().toString());

        db.update(Utilidades.TABLA_PERONAS,values,Utilidades.CAMPO_ID+"=?",parametros);
        Toast.makeText(this,"Los datos se actualizaron!",Toast.LENGTH_SHORT).show();
        campoId.setText("");
        apellido.setText("");
        telefono.setText("");
        nombre.setText("");
        db.close();
    }

    private void consultar() {
        SQLiteDatabase db = conn.getWritableDatabase();
        String[] parametros = {campoId.getText().toString()};
        String[] cmapos = {Utilidades.CAMPO_NOMBRE,Utilidades.CAMPO_APELLIDO,Utilidades.CAMPO_TELEFONO};

        try {
            Cursor cursor = db.query(Utilidades.TABLA_PERONAS,cmapos,Utilidades.CAMPO_ID+"=?",parametros,null,null,null);
            cursor.moveToFirst();
            nombre.setText(cursor.getString(0));
            apellido.setText(cursor.getString(1));
            telefono.setText(cursor.getString(2));
            db.close();
        }catch (Exception e){
            Toast.makeText(this,"El usuario que buscas no existe",Toast.LENGTH_SHORT).show();
            campoId.setText("");
            apellido.setText("");
            telefono.setText("");
            nombre.setText("");
            db.close();
        }
    }
}
