package foreverprogramacion.aplicacionsqllite;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.internal.BottomNavigationItemView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import foreverprogramacion.aplicacionsqllite.Utilidades.Utilidades;

public class insert extends AppCompatActivity {

    EditText idPersona,nombre,apellido,telefono;
    Button insertar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        idPersona = (EditText) findViewById(R.id.campoId);
        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
        telefono = (EditText) findViewById(R.id.telefono);

        insertar = (Button) findViewById(R.id.insert);
    }

    public void registrar(View view) {
        if (idPersona.equals("") && nombre.equals("") && apellido.equals("") && telefono.equals("")){
            Toast.makeText(this,"Los campos no deben estar vacios",Toast.LENGTH_SHORT).show();
        }else{registrarUsuario();}
    }

    private void registrarUsuario() {
        //Conexion a BD
        conexionHelper conn = new conexionHelper(this,"bd_personas",null,1);
        //Usar la base de datos
        SQLiteDatabase db = conn.getWritableDatabase();
        //Empaquetar valores para el insert
        ContentValues values = new ContentValues();
        values.put(Utilidades.CAMPO_ID,idPersona.getText().toString());
        values.put(Utilidades.CAMPO_NOMBRE,nombre.getText().toString());
        values.put(Utilidades.CAMPO_APELLIDO,apellido.getText().toString());
        values.put(Utilidades.CAMPO_TELEFONO,telefono.getText().toString());
        //Ejecutar insert, en caso de exito regresera ID
        long idResultado = db.insert(Utilidades.TABLA_PERONAS,Utilidades.CAMPO_ID,values);

        Toast.makeText(getApplicationContext(),"Exito: "+idResultado+" es su ID.",Toast.LENGTH_SHORT).show();

        idPersona.setText("");
        nombre.setText("");
        apellido.setText("");
        telefono.setText("");
    }


}
