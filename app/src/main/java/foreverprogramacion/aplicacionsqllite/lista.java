package foreverprogramacion.aplicacionsqllite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import foreverprogramacion.aplicacionsqllite.Tablas.personas;
import foreverprogramacion.aplicacionsqllite.Utilidades.Utilidades;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class lista extends AppCompatActivity {

    conexionHelper conn;
    ListView listViewPersonas;
    ArrayList<String> listaInformacion;
    ArrayList<personas> listaUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        conn = new conexionHelper(this,"bd_personas",null,1);
        listViewPersonas = (ListView) findViewById(R.id.listViewPersonas);

        consultarPersonas();

        ArrayAdapter adaptador = new ArrayAdapter(this,android.R.layout.simple_list_item_1,listaInformacion);
        listViewPersonas.setAdapter(adaptador);
    }

    private void consultarPersonas(){
        SQLiteDatabase db = conn.getWritableDatabase();
        personas Personas = null;

        listaUsuarios = new ArrayList<personas>();
        Cursor cursor = db.rawQuery("SELECT * FROM "+ Utilidades.TABLA_PERONAS,null);

        while (cursor.moveToNext()){
            Personas = new personas();
            Personas.setId(cursor.getInt(0));
            Personas.setNombre(cursor.getString(1));
            Personas.setApellido(cursor.getString(2));
            Personas.setTelefono(cursor.getString(3));

            listaUsuarios.add(Personas);
        }

        obtenerLista();
    }

    private void obtenerLista() {
        listaInformacion = new ArrayList<String>();

        for (int i=0;i<listaUsuarios.size();i++){
            listaInformacion.add("ID: "+listaUsuarios.get(i).getId()+"\n"+
                    "Nombre: "+listaUsuarios.get(i).getNombre()+" "+listaUsuarios.get(i).getApellido()+"\n"+
                    "Telefono: "+listaUsuarios.get(i).getTelefono());
        }
    }
}
